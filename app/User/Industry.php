<?php

namespace App\User;

use Illuminate\Database\Eloquent\Model;

class Industry extends Model
{
    protected $fillable = [
        'nama_usaha', 'nama_pemilik', 'nik','alamat_pemilik','alamat_usaha','no_telp','no_hp','email','npwp','jenis_usaha'
    ];
}
