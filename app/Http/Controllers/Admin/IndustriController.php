<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User\Industry;

class IndustriController extends Controller
{
    //
    public function index()
    {
        # code...
        $industries = Industry::all();
        // dd($industries);
        return view('admin.industri.index')->with(['industries'=>$industries]);
    }

}
