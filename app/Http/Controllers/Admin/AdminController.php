<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use Auth;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.index');
    }

    public function users()
    {
        $users = User::all();
        return view('admin.user.index')->with('users',$users);
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::all();
        // dd($user);
        return view('admin.user.edit')->with(['user'=>$user, 'roles'=>$roles]);
    }

    public function tambah(){
        return view('admin.user.tambah');
    }
    public function show($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::all();
        // dd($user);
        return view('admin.user.edit')->with(['user'=>$user, 'roles'=>$roles]);
        
    }

    public function store(Request $request)
    {
        // $user = new User;
        // $user->name = $request->name;
        // $user->email = $request->email;
        // $user->password = bcrypt($request->password);
        return response()->json(['success'=>'Akun berhasil dibuat ']);

    }

    public function update($id,Request $request)
    {
        $request->validate([
            'name' => ['required'],
            'role' => ['required']
        ]);
        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->role_id = $request->role;
        $user->save();
        return redirect()->route('admin.user.index');
        
        // if($request->new_password !== null){
        //     if($user->password == bcrypt($request->old_password)){
        //         // $user->password = bcrypt($request->new_password);
        //         $user->password = $request->new_password;
        //     }
        // }
        // dd(Hash::check($request->new_password, bcrypt($user->password)));
        // dd($user, $request->new_password, bcrypt($request->new_password),bcrypt($request->old_password));
    }

    public function delete($id)
    {
        if (Auth::user()->id !== intval($id)) {
            $user = User::find($id);
            $user->delete();
            return redirect()->route('admin.users');
        }
        return redirect()->route('admin.users');
        
    }
    
}
