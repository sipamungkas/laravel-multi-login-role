<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User\Industri;
use Auth;

class UserController extends Controller
{
    //
    public function index()
    {
        // dd(Auth::user()->industri);
        $industri = Auth::user()->industri;
        // dd($industri);
        // dd(array_random(['tekstil','makanan','jasa']));
        return view('user.index')->with(['industri'=>$industri]);
    }
}
