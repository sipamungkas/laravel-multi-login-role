<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User\Industry;
use App\User;
use Auth;

class IndustriController extends Controller
{
    //
    public function daftar()
    {
        return view('user.industri.daftar');
    }

    public function store(Request $request)
    {
        $industri = new Industry($request->all());
        $industri->save();
        $user = User::find(Auth::user()->id);
        $user->industri_id = $industri->id;
        $user->save();
        return redirect()->route('user.index');

    }
}
