<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$role)
    {
        if (in_array(Auth::user()->role->name,$role)) {
            // dd(Auth::user()->name,Auth::user()->role->name);
            return $next($request);
        };
        // dd('error');
        return redirect()->route('login');
    }
}
