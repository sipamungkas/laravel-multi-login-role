<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\User;
use App\User\Industry;

class UserIndustrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //set faker to Indonesia
        $faker = Faker::create('id_ID');

        for ($i=0; $i < 10; $i++) { 
            $data = Industry::create([
                'nama_usaha' => 'UKM Mandiri '.$i,
                'nama_pemilik' => $faker->name,
                'nik' => '12345678912',
                'alamat_pemilik' => $faker->address,
                'alamat_usaha' => $faker->address,
                'no_telp' => $faker->phoneNumber,
                'no_hp' => $faker->phoneNumber,
                'email' => $faker->safeEmail,
                'npwp' => 12345678901234,
                'jenis_usaha' => array_random(['tekstil','makanan','jasa']),
                'status' => 0
            ]);

            $user = User::create([
                'name' => $faker->name,
                'email' => $faker->safeEmail,
                'password' => bcrypt('user1234'),
                'role_id' => 2,
                'industri_id' => $data->id
            ]);
        }
    }
}
