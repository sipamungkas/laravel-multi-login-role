<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
            'name'=>'Ragil Burhanudin Pamungkas',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin123'),
            'role_id' =>1,
        ]);
        User::create([
            'name'=>'Burhanudin',
            'email' => 'user@user.com',
            'password' => bcrypt('user1234'),
            'role_id' =>1,
        ]);
    }
}
