<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndustriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('industries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_usaha');
            $table->string('nama_pemilik');
            $table->bigInteger('nik');
            $table->text('alamat_pemilik');
            $table->text('alamat_usaha');
            $table->string('no_telp');
            $table->string('no_hp');
            $table->string('email');
            $table->bigInteger('npwp');
            $table->string('jenis_usaha');
            $table->boolean('status')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('industries');
    }
}
