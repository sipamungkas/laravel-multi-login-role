<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('auth','checkRole:admin')->group(function(){
    Route::get('/','AdminController@index')->name('index');
    Route::get('/users','AdminController@users')->name('user.index');
    Route::get('/user/{id}/edit','AdminController@edit')->name('user.edit');
    Route::get('/user/tambah','AdminController@tambah')->name('user.tambah');
    Route::post('user/tambah','AdminController@store')->name('user.store');
    Route::get('/user/{id}/delete','AdminController@delete')->name('user.delete');
    Route::get('/user/{id}','AdminController@show')->name('user.show');
    Route::put('/user/{id}','AdminController@update')->name('user.update');
    Route::get('/industri','IndustriController@index')->name('industri.index');
});

Route::namespace('User')->prefix('user')->name('user.')->middleware('auth','checkRole:user')->group(function(){
    Route::get('/','UserController@index')->name('index');
    Route::get('/industri/daftar','IndustriController@daftar')->name('industri.daftar');
    Route::post('industri/daftar','IndustriController@store')->name('industri.save');
});