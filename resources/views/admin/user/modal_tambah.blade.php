<div class="modal fade" id="modal-tambah" data-backdrop="static" tabindex="-1" role="dialog"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 col-12">
                        <div class="card-body">
                            <form id='myForm'>
                                @csrf
                                <div class="form-group">
                                    <label for="noTelp">Nama</label>
                                    <input name="name" type="text" class="form-control" id="name"
                                        placeholder="Masukkan Nama Lengkap Anda">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input name="email" type="email" class="form-control" id="email"
                                        placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <label for="password">Email</label>
                                    <input name="password" type="password" class="form-control" id="password"
                                        placeholder="Password">
                                </div>
                                {{-- <button class="btn btn-success float-right" id="ajaxSubmit">Tambah</button> --}}
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-success" id="ajaxSubmit">Tambah</button>
                </div>
            </div>
        </div>
    </div>