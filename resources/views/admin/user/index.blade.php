@extends('layouts.admin')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Dashboard</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Dashboard v1</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <!-- Main row -->
    <div class="row">
      <!-- Left col -->
      <section class="col-lg-12 col-sm-12 col-12">
        <!-- Custom tabs (Charts with tabs)-->
        <div class="card">
          <div class="card-header">
            <div class="d-flex justify-content-between align-items-center">
              <h3 class="card-title">
                User
              </h3>
              {{-- <a class="btn btn-info btn-sm float-right" href="{{route('admin.user.tambah')}}"> + Tambah</a> --}}
              <button type="button" class="btn btn-info btn-sm float-right" data-toggle="modal"
                data-target="#modal-tambah"> + Tambah</button>
            </div>

          </div><!-- /.card-header -->
          <div class="card-body">
            <table id="example2" class="table table-bordered table-hover dataTable" role="grid"
              aria-describedby="example2_info">
              <thead>
                <tr role="row">
                  <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                    aria-sort="ascending" aria-label="#">#</th>
                  <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Nama">
                    Nama</th>
                  <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Email">
                    Email</th>
                  <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Role">
                    Role</th>
                  <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Action">
                    Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($users as $user)

                <tr role="row">
                  <td>{{$loop->iteration}}</td>
                  <td>{{$user->name}}</td>
                  <td>{{$user->email}}</td>
                  <td>{{$user->role->name}}</td>
                  <td><a class="btn btn-warning btn-sm" href="{{route('admin.user.edit',$user->id)}}">Edit</a>
                    <a href="{{route('admin.user.delete',$user->id)}}" class="btn btn-danger btn-sm">Hapus</a></td>
                </tr>
                @endforeach

              </tbody>
            </table>
          </div><!-- /.card-body -->
        </div>
        <!-- /.card -->

      </section>
      <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->
</section>
@include('admin.user.modal_tambah')
<!-- /.content -->
@endsection
@section('script')
<script>
  $(function () {
    $("#example2").DataTable();
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
  
  $(document).ready(function(){
    $('#ajaxSubmit').click(function(e){
      e.preventDefault();
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: "{{route('admin.user.store')}}",
        method: "post",
        data: {
          'name': $('#name').val(),
          'email' : $('#email').val(),
          'password' : $('#password').val()
        },
        success: function(res){
          console.log(res);
          $('#modal-tambah').modal('toggle');
          toastr["success"](res.success, "Sukses");
        }
      })
    })
  })
  toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

</script>
@endsection