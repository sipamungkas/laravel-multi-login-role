@extends('layouts.admin')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Dashboard</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Dashboard v1</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-lg-12 col-12">
        <div class="card">
          <div class="card-header border-1">
            <div class="d-flex justify-content-between align-items-center">
              <h3 class="card-title">Tambah Akun</h3>
              <a href="{{route('admin.user.index')}}" class="btn btn-info btn-sm">Batal</a>
            </div>
          </div>
          <div class="card-body">
            <form id='myForm'>
              @csrf
              <div class="form-group">
                <label for="noTelp">Nama</label>
                <input name="name" type="text" class="form-control" id="name" placeholder="Masukkan Nama Lengkap Anda">
              </div>
              <div class="form-group">
                <label for="email">Email</label>
                <input name="email" type="email" class="form-control" id="email" placeholder="Email">
              </div>
              <div class="form-group">
                <label for="password">Email</label>
                <input name="password" type="password" class="form-control" id="password" placeholder="Password">
              </div>
              <button class="btn btn-success float-right" id="ajaxSubmit">Tambah</button>
            </form>
          </div>
        </div>
      </div>
      <!-- /.col-md-6 -->
    </div>
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection

@section('script')
<script>
  $(document).ready(function(){
    $('#ajaxSubmit').click(function(e){
      e.preventDefault();
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: "{{route('admin.user.store')}}",
        method: "post",
        data: {
          'name': $('#name').val(),
          'email' : $('#email').val(),
          'password' : $('#password').val()
        },
        success: function(res){
          console.log(res);
          toastr["success"](res.success, "Sukses")
        }
      })
    })
  })
  toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
</script>

@endsection