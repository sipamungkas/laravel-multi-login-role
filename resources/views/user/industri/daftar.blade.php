@extends('layouts.user')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Pendaftaran Industri</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">User</a></li>
                    <li class="breadcrumb-item"><a href="#">Industri</a></li>
                    <li class="breadcrumb-item active">Daftar</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 col-sm-12 col-12">
                {{-- kanan --}}
                <form method="POST" action="{{route('user.industri.save')}}">
                    @csrf
                    <div class="row">
                        <div class="col-lg-6 col-sm-12 col-6">
                            <div class="card">
                                {{-- <div class="card-header">
                            <h3 class="card-title">
                                Profil Industri
                            </h3>
                        </div><!-- /.card-header --> --}}
                                {{-- .card-body --}}
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="namaUsaha">Nama Usaha</label>
                                        <input type="text" name="nama_usaha" class="form-control" id="namaUsaha"
                                            placeholder="Nama Usaha">
                                    </div>
                                    <div class="form-group">
                                        <label for="namaPemilik">Nama Pemilik</label>
                                        <input name="nama_pemilik" type="text" class="form-control" id="namaPemilik"
                                            placeholder="Nama Pemilik">
                                    </div>
                                    <div class="form-group">
                                        <label for="nikPemilik">NIK Pemilik</label>
                                        <input name="nik" type="integer" class="form-control" id="nikPemilik"
                                            placeholder="NIK Pemilik">
                                    </div>
                                    <div class="form-group">
                                        <label for="alamatPemilik">Alamat Pemilik</label>
                                        <input name="alamat_pemilik" type="text" class="form-control" id="alamatPemilik"
                                            placeholder="Alamat Pemilik">
                                    </div>
                                    <div class="form-group">
                                        <label for="alamatUsaha">Alamat Usaha</label>
                                        <input name="alamat_usaha" type="text" class="form-control" id="alamatUsaha"
                                            placeholder="Alamat Usaha">
                                    </div>
                                </div>
                                {{-- /.card-body --}}
                            </div>
                            <!-- /.card -->
                        </div>
                        {{-- kiri --}}
                        <div class="col-lg-6 col-sm-12 col-6">
                            <div class="card">
                                {{-- <div class="card-header">
                            <h3 class="card-title">
                                Profil Industri
                            </h3>
                        </div><!-- /.card-header --> --}}

                                {{-- .card-body --}}
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="noTelp">No. Telepon</label>
                                        <input name="no_telp" type="text" class="form-control" id="noTelp"
                                            placeholder="No. Telepon">
                                    </div>
                                    <div class="form-group">
                                        <label for="noHp">No. HP</label>
                                        <input name="no_hp" type="text" class="form-control" id="noHp"
                                            placeholder="No. HP">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input name="email" type="email" class="form-control" id="email"
                                            placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <label for="NPWP">NPWP</label>
                                        <input name="npwp" type="text" class="form-control" id="NPWP"
                                            placeholder="NPWP">
                                    </div>
                                    <div class="form-group">
                                        <label for="jenisUsaha">Jenis Usaha</label>
                                        <input name="jenis_usaha" type="text" class="form-control" id="jenisUsaha"
                                            placeholder="Jenis Usaha">
                                    </div>
                                </div>
                                {{-- /.card-body --}}
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                    <input class="btn btn-info float-right" type="submit" value="Simpan" />
                </form>
            </section>
            <!-- /.Left col -->
            <div class="clear-fix"></div>

        </div>
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

@endsection