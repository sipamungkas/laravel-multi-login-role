@extends('layouts.user')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Profil Industri</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard v1</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 col-sm-12 col-12">
                <!-- Custom tabs (Charts with tabs)-->
                <div class="card">
                    @if ($industri !== null)
                    <div class="row">
                        <div class="col-lg-6 col-sm-12 col-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="namaUsaha">Nama Usaha</label>
                                        <input type="text" name="nama_usaha" class="form-control" id="namaUsaha"
                                            placeholder="Nama Usaha" value="{{$industri->nama_usaha}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="namaPemilik">Nama Pemilik</label>
                                        <input name="nama_pemilik" type="text" class="form-control" id="namaPemilik"
                                            placeholder="Nama Pemilik" value="{{$industri->nama_pemilik}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="nikPemilik">NIK Pemilik</label>
                                        <input name="nik" type="integer" class="form-control" id="nikPemilik"
                                            value="{{$industri->nik}}" placeholder="NIK Pemilik">
                                    </div>
                                    <div class="form-group">
                                        <label for="alamatPemilik">Alamat Pemilik</label>
                                        <input name="alamat_pemilik" type="text" class="form-control" id="alamatPemilik"
                                            placeholder="Alamat Pemilik" value="{{$industri->alamat_pemilik}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="alamatUsaha">Alamat Usaha</label>
                                        <input name="alamat_usaha" type="text" class="form-control" id="alamatUsaha"
                                            placeholder="Alamat Usaha" value="{{$industri->alamat_usaha}}">
                                    </div>
                                </div>
                                {{-- /.card-body --}}
                            </div>
                            <!-- /.card -->
                        </div>
                        {{-- kiri --}}
                        <div class="col-lg-6 col-sm-12 col-6">
                            <div class="card">
                                {{-- <div class="card-header">
                            <h3 class="card-title">
                                Profil Industri
                            </h3>
                        </div><!-- /.card-header --> --}}

                                {{-- .card-body --}}
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="noTelp">No. Telepon</label>
                                        <input name="no_telp" type="text" class="form-control" id="noTelp"
                                            placeholder="No. Telepon" value="{{$industri->no_telp}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="noHp">No. HP</label>
                                        <input name="no_hp" type="text" class="form-control" id="noHp"
                                            placeholder="No. HP" value="{{$industri->no_hp}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input name="email" type="email" class="form-control" id="email"
                                            placeholder="Email" value="{{$industri->email}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="NPWP">NPWP</label>
                                        <input name="npwp" type="text" class="form-control" id="NPWP" placeholder="NPWP"
                                            value="{{$industri->npwp}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="jenisUsaha">Jenis Usaha</label>
                                        <input name="jenis_usaha" type="text" class="form-control" id="jenisUsaha"
                                            placeholder="Jenis Usaha" value="{{$industri->jenis_usaha}}">
                                    </div>
                                </div>
                                {{-- /.card-body --}}
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                    @else
                    <div class="card-body">
                        <center>
                            <div class="col-12">
                                Anda belum memiliki profil Industri
                            </div>
                            <div class="col-12"><a class="btn btn-info" href="{{route('user.industri.daftar')}}"> Buat
                                    Profil
                                    Industri </a>
                            </div>
                        </center>

                    </div>
                    @endif

                </div>
                <!-- /.card -->
            </section>
            <!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

@endsection